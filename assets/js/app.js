import Vue from "vue/dist/vue.esm.js";

const users = [
  "dem",
  "yasu",
  "miyata",
  "genta",
  "adit",
  "hiraku",
  "miyako",
  "natsumi",
  "yukiho",
  "kyosuke",
  "tamai",
  "jay",
  "kaito",
  "ryusei",
  "hiroshi",
  "kumakura",
  "nagai",
  "ryo",
  "matsuoka",
  "fukushima",
];

const langs = [
  "楽しく",
  "激しく",
  "元気に",
  "クールに",
  "出村っぽく",
  "荻野っぽく",
  "宮田っぽく",
  "面白い感じで",
];

var drum = document.getElementById("drum");
var cymbal = document.getElementById("cymbal");

var usertimer;
var langtimer;

let info = new Vue({
  el: "#app",
  components: {},
  data: {
    users: [],
    langs: [],
    selectedLang: "",
    selectedUser: "",
    selectedUserId: 0,
    selectedLangId: 0,
    ready: false,
    done: false,
    slot: false,
  },
  mounted: function() {
    this.users = users;
    this.selectedUser = users[0];
    this.langs = langs;
    this.selectedLang = langs[0];
    this.ready = true;
  },
  created: function() {},
  methods: {
    slotStart: function() {
      this.slot = true;
      this.done = false;
      usertimer = setInterval(() => {
        this.selectedUserId += 1;
        if (this.selectedUserId == this.users.length) {
          this.selectedUserId = 0;
        }
        this.selectedUser = this.users[this.selectedUserId];
      }, 40);

      langtimer = setInterval(() => {
        this.selectedLangId += 1;
        if (this.selectedLangId == this.langs.length) {
          this.selectedLangId = 0;
        }
        this.selectedLang = this.langs[this.selectedLangId];
      }, 30);

      drum.currentTime = 0;
      drum.play();
      console.log("start");
    },
    slotStop: function() {
      drum.pause();
      cymbal.currentTime = 0;
      cymbal.play();
      clearInterval(usertimer);
      clearInterval(langtimer);
      this.slot = false;
      this.done = true;
    },
    toggleSlot: function() {
      setTimeout(function() {});
    },
  },
  filters: {},
});
