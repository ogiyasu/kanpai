const path = require('path');
const webpack = require('webpack');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const globule = require('globule')
const HtmlWebpackPlugin = require('html-webpack-plugin');
const VueLoaderPlugin = require("vue-loader/lib/plugin")

const opts = {
  srcDir: path.join(__dirname, 'assets'),
  destDir: path.join(__dirname, 'public')
}

const from = 'pug';
const to = 'html';
const htmlPluginConfig = globule.find([`**/*.${from}`, `!**/_*.${from}`], {cwd: opts.srcDir}).map(filename => {
  const file = filename.replace(new RegExp(`.${from}$`, 'i'), `.${to}`).split('/')
  return new HtmlWebpackPlugin({
    filename: filename.replace(new RegExp(`.${from}$`, 'i'), `.${to}`).replace(/(\.\/)?pug/, '.'),
    template: `./${filename}`
  })
})

module.exports = (env, argv) => {
  const IS_DEVELOPMENT = argv.mode === 'development';
  return {
    context: opts.srcDir,
    entry: {
      app: ['./js/app.js', './scss/app.scss']
    },
    output: {
      path: path.resolve(__dirname, 'public'),
    },
    devtool: IS_DEVELOPMENT ? 'inline-source-map' : false,
    module: {
      rules: [
        {
          test: /\.pug$/,
          use: [
            {
              loader: 'pug-loader',
              options: {
                pretty: true,
              }
            }
          ],
        },
        {
          test:/.js$/,
          exclude: '/node_modules/',
          use: [
            'babel-loader'
          ]
        },
        {
          test: /.vue$/,
          exclude: '/node_modules/',
          use: [
            {
              loader: 'vue-loader',
              options: {
                // extractCSS: true,
                loaders: {
                  js: 'babel-loader'
                }
              }
            }
          ]
        },
        {
          test: /\.(scss|css)$/,
          use: [
            {
              loader: MiniCssExtractPlugin.loader,
            },
            {
              loader: 'css-loader',
              options: {
                sourceMap: IS_DEVELOPMENT,
                //url: false
              }
            },
            {
              loader: 'postcss-loader',
              options: {
                sourceMap: IS_DEVELOPMENT,
                plugins: () => [
                  require('autoprefixer')()
                ],
              }
            },
            {
              loader: 'sass-loader',
              options: { sourceMap: IS_DEVELOPMENT, }
            }
          ]
        }
      ]
    },
    plugins: [
      ...htmlPluginConfig,
      new MiniCssExtractPlugin({filename: './css/[name].css'}),
      new VueLoaderPlugin()
    ]
  }
};
